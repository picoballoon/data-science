from setuptools import find_packages, setup

setup(
    name='picoballoon',
    packages=find_packages(include=["picoballoon-api-lib"]),
    version='0.1.0',
    description='The API library for picoballoon',
    author='Kalle Bracht',
    license='MIT',
    install_requires=[
        "requests",
        "datetime",
        "typing"
    ],
)