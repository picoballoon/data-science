import requests
import json
from datetime import datetime
from typing import List


class Packet:
    """
    Constructor of the packet class. Every measure data has packets. Here is the class for this.

    :parameter measure_id: The measure ID to identify the packet
    """
    def __init__(self, measure_list: dict):
        self.__measure_list__ = measure_list

    """
    STR of class if printed the data will print as dict

    :return: dict of measure data of a packet print as string
    """
    def __str__(self):
        return str(self.__measure_list__)

    """
    Gets all data in a packet.

    :return: All packet data of packet(ID)
    """
    def all(self):
        return self.__measure_list__

    """
    Gets the coordinates of the packet.

    :returns: Coordinates lon and Coordinates lat as float
    """
    def getCoordinates(self) -> [float, float]:
        return float(self.__measure_list__["coordinates_lat"]), float(self.__measure_list__["coordinates_lon"])

    """
    Gets the datarate of the packet.

    :return: Returns datarate of the packet as string
    """
    def getDataRate(self) -> str:
        return self.__measure_list__["drsf"]

    """
    Gets the measure ID of the packet.

    :return: Returns the measure ID of the packet as integer
    """
    def getMeasureID(self) -> int:
        return self.__measure_list__["measure_id"]

    """
    Gets the mission ID of the packet.

    :return: Returns the mission ID of the packet as integer
    """
    def getMissionID(self) -> int:
        return self.__measure_list__["mission_id"]

    """
    Gets the mission ID of the packet.

    :return: Returns the mission ID of the packet as integer
    """
    def getMeasureTimestamp(self) -> datetime:
        return datetime.strptime(self.__measure_list__["mission_id"].replace(",", ""), "%a %d %b %Y %X %Z")

    """
    Gets the altitude of the packet.
    
    :return: Returns the altitude of the packet as float
    """
    def getAltitude(self) -> float:
        return self.__measure_list__["sensor_data"]["altitude"]

    """
    Gets the humidity of the packet.

    :return: Returns the humidity of the packet as float
    """
    def getHumidity(self) -> float:
        return self.__measure_list__["sensor_data"]["humidity"]

    """
    Gets the pressure of the packet.

    :return: Returns the pressure of the packet as float
    """
    def getPressure(self) -> float:
        return self.__measure_list__["sensor_data"]["pressure"]

    """
    Gets the solar panel voltage of the packet.

    :return: Returns the solar panel voltage of the packet as float
    """
    def getSolarPanelVoltage(self) -> float:
        return self.__measure_list__["sensor_data"]["solar_panel_voltage"]

    """
    Gets the speed of the packet.

    :return: Returns the speed of the packet as float
    """
    def getSpeed(self) -> float:
        return self.__measure_list__["sensor_data"]["speed"]

    """
    Gets the temperature of the packet.

    :return: Returns the temperature of the packet as float
    """
    def getTemperature(self) -> float:
        return self.__measure_list__["sensor_data"]["temp"]

    """
    Gets the UV radiation of the packet.

    :return: Returns the UV radiation of the packet as float
    """
    def getUVRadiation(self) -> float:
        return self.__measure_list__["sensor_data"]["uv_Radiation"]

    """
    Gets the time to fix of the packet.

    :return: Returns the time to fix of the packet as float
    """
    def getTimeToFix(self) -> float:
        return self.__measure_list__["sensor_data"]["time_to_fix"]

    """
    Gets the sensor data. Returns a sensor dict.
    
    :return: Returns the sensor data as dict
    """
    def getSensors(self) -> dict:
        return self.__measure_list__["sensor_data"]

    """
    Gets the gateway data of the packet.
    
    :return: Returns a list of gateways as dict 
    """
    def getGateways(self) -> List[dict]:
        return self.__measure_list__["gateway_data"]



class Measure:
    """
    Gets the measure data form api.picoballoon.org

    :return: The all measure data of the probe
    """
    def __getmeasure__(self) -> dict:
        responde = requests.get(f"https://api.picoballoon.org/measure_data/by_probe_id?id={self.mission_id}")

        return json.loads(responde.text)["payload"]

    """
    Constructor of the measure class. It is for the measure data of picoballoon.
            
    :parameter measure_id: The measure ID of the probe. Required to identify the measure packet 
    """
    def __init__(self, mission_id: int):
        self.mission_id = mission_id

    """
    STR of class if printed the data will print as dict

    :return: dict of measure data print as string
    """
    def __str__(self):
        return str(self.__getmeasure__())

    """
    Gets all measured data from an mission by mission ID.
    
    :return: All measure data of one probe identified by mission ID
    """
    def all(self) -> dict:
        return self.__getmeasure__()

    """
    Gets a packet by the mission ID.
    
    :parameter measure_id: The measure ID to identify the packet
    
    :return: Measure packet in dict format
    """
    def packet(self, measure_id: int) -> Packet:
        measure_list = next((item for item in self.__getmeasure__() if item['measure_id'] == measure_id), None)

        if measure_list == None:
            raise ValueError("Packet does not exist")

        return Packet(measure_list)

    """
    Gets all packets. Returns a list.
    
    :return: Returns a list of packet as packet class
    """
    def packets(self) -> List[Packet]:
        packet_list = []
        for i in self.__getmeasure__():
            packet_list.append(Packet(i))

        return packet_list


class Picoballoon:
    """
    Gets the probe form api.picoballoon.org

    :return: The general probe information of the probe
    """
    def __getprobe__(self) -> dict:
        responde = requests.get(f"https://api.picoballoon.org/general_probe_data/by_probe_id?id={self.mission_id}")

        try:
            return json.loads(responde.text)["payload"][0]
        except IndexError:
            raise ValueError("Picoballoon does not exit")

    """
    Constructor of the picoballoon class. Gets also general probe data. Measure data will get by separate method.

    :parameter mission_id: The mission ID, required to identify the probe
    """
    def __init__(self, mission_id: int):
        self.mission_id = mission_id

        self.__general_probe_data__ = self.__getprobe__()

    """
    STR of class if printed the data will print as dict
    
    :return: dict of general probe data print as string
    """
    def __str__(self):
        return str(self.__general_probe_data__)

    """
    Returns the balloon mass of the mission.

    :return: Returns the balloon mass as float
    """
    def getBalloonMass(self) -> float:
        return float(self.__general_probe_data__["balloon_mass"])

    """
    Returns the balloon version of the mission.

    :return: Returns the balloon version as string
    """
    def getBalloonVersion(self) -> str:
        return str(self.__general_probe_data__["balloon_version"])

    """
    Returns the firmware version of the probe.

    :return: Returns the probe firmware version as string
    """
    def getFirmwareVersion(self) -> str:
        return str(self.__general_probe_data__["balloon_mass"])

    """
    Returns the launch time of the mission.

    :return: Returns the launch time as datetime
    """
    def getLaunchTime(self) -> datetime:
        return datetime.strptime(self.__general_probe_data__["launch_time"].replace(",", ""), "%a %d %b %Y %X %Z")

    """
    Returns the mission ID of the probe.

    :return: Returns the mission ID as integer
    """
    def getMissionID(self) -> int:
        return self.mission_id

    """
    Returns the mission name of the mission.

    :return: Returns the mission name as string
    """
    def getMissionName(self) -> str:
        return str(self.__general_probe_data__["mission_name"])

    """
    Returns the mission status of the mission.

    :return: Returns the mission status as string
    """
    def getMissionStatus(self) -> str:
        return str(self.__general_probe_data__["mission_status"])

    """
    Returns the probe mass of the probe.

    :return: Returns the probe mass as float
    """
    def getProbeMass(self) -> float:
        return float(self.__general_probe_data__["probe_mass"])

    """
    Returns the probe version of the probe.

    :return: Returns the probe version as float
    """
    def getProbeVersion(self) -> float:
        return float(self.__general_probe_data__["probe_version"])

    """
    Creates a measure class for measure data. 

    :return: Pointer of a measure class
    """
    def Measure(self) -> Measure:
        return Measure(self.mission_id)


